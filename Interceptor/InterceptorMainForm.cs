﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Interceptor
{
    public partial class InterceptorMainForm : Form
    {
        private const int CUTOFF_TRIES = 10;
        private const int SCREENSHOT_DELAY = 2;

        private System.Timers.Timer screenshotTimer;
        private BackgroundWorker screenshotBackGroundWorker;

        private string interceptorDir;
        private string screenshotDir;
        private string settingsFile;

        private string apiKey;

        public InterceptorMainForm()
        {
            InitializeComponent();

            screenshotBackGroundWorker = new BackgroundWorker();

            screenshotBackGroundWorker.DoWork += backgroundWorker1_processScreenshot;

            apiKey = null;

            screenshotTimer = new System.Timers.Timer(SCREENSHOT_DELAY);
        }

        private string takeScreenshot(string screenshot_dir)
        {
            string imageFilePath = null;

            Rectangle bounds = Screen.GetBounds(Point.Empty);

            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }

                //Replace all invalid file path characters with dashes.
                Regex r = new Regex(@"[\/\:\s]+");
                string cleanDateString = r.Replace(DateTime.UtcNow.ToString(), "-");

                imageFilePath = System.IO.Path.Combine(screenshot_dir, cleanDateString + "-GMT.png");

                Console.WriteLine("Saving to \"" + imageFilePath + "\"");

                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(imageFilePath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        bitmap.Save(memory, ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
            }

            return imageFilePath;
        }

        private string performOCRScreenshot(string imageFilePath)
        {
            Process tesseractProcess = new Process();

            tesseractProcess.StartInfo.FileName = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe";
            tesseractProcess.StartInfo.Arguments = imageFilePath + " - --oem 1 -l eng";
            tesseractProcess.StartInfo.UseShellExecute = false;
            tesseractProcess.StartInfo.RedirectStandardOutput = true;
            tesseractProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            tesseractProcess.StartInfo.CreateNoWindow = true;

            tesseractProcess.Start();

            tesseractProcess.WaitForExit();

            string text = tesseractProcess.StandardOutput.ReadToEnd();

            return text;
        }

        private bool textIsHarmful(string text)
        {
            string[] badWords = { "nudes", "school shooter", "school shooting", "kill myself" };

            foreach (string badWord in badWords)
            {
                if (text.IndexOf(badWord) >= 0)
                {
                    return true;
                }
            }

            return false;
        }

        private void scheduleTimer()
        {
            screenshotTimer.Elapsed += new System.Timers.ElapsedEventHandler(processScreenshot);
            screenshotTimer.Start();
        }

        private string sendScreenshot(string url, string path)
        {
            FileStream imageFile = null;
            BinaryReader br = null;

            int tries = 1;

            while (tries <= CUTOFF_TRIES)
            {
                try
                {
                    //Read image from file (path)
                    imageFile = new FileStream(path, FileMode.Open, FileAccess.Read);

                    br = new BinaryReader(imageFile);

                    int fileSize = (int)new FileInfo(path).Length; //NOTE: Cast assumes that files are less than 2GB in size.

                    byte[] imageData = br.ReadBytes(fileSize);

                    //Base64 encode image.
                    string base64ImageData = Convert.ToBase64String(imageData);

                    //send image to http://localhost:12345/classify_text using HTTP POST request as "b64_img" element in form.
                    using (WebClient client = new WebClient())
                    {
                        var values = new NameValueCollection();

                        values.Add("b64_img", base64ImageData);

                        var response = client.UploadValues(url, values);
                    }

                    //REMEMBER TO TURN SERVER ON BEFORE RUNNING THIS!
                    break;
                }
                catch (System.Net.WebException)
                {
                    if (tries == CUTOFF_TRIES)
                    {
                        return "Sorry! Interceptor is unable to access its server.\nPlease confirm that your computer has Internet access and restart Interceptor.\nIf the problem persists, please contact Interceptor tech support.\n";
                    }
                    else
                    {
                        Console.WriteLine("Interceptor is unable to access its server. Retrying for " + tries + " time.");
                    }
                }
                finally
                {
                    if (br != null)
                    {
                        br.Close();
                    }

                    if (imageFile != null)
                    {
                        imageFile.Close();
                    }
                }

                tries += 1;
            }

            return null;
        }

        private void processScreenshot(object sender, System.Timers.ElapsedEventArgs e)
        {
            screenshotTimer.Stop();

            string screenshotPath = null;
            int tries = 0;

            while (tries < CUTOFF_TRIES)
            {
                try
                {
                    screenshotPath = takeScreenshot(screenshotDir);

                    break;
                }
                catch (IOException)
                {
                    tries += 1;

                    if (tries == CUTOFF_TRIES)
                    {
                        exitApplication("Sorry! Interceptor could not take a screenshot. Please restart Interceptor.");
                    }
                }
            }

            bool receivedError = false;
            string responseStr = null;

            if (screenshotPath != null)
            {
                responseStr = sendScreenshot("http://localhost:12345/classify_text/", screenshotPath);

                if (responseStr != null)
                {
                    Console.WriteLine("Received response: \"" + responseStr + "\"");
                    receivedError = true;
                }

                //TODO: Check for exceptions (esp. permission issues).
                if (File.Exists(screenshotPath))
                {
                    new FileInfo(screenshotPath).Delete();
                }
            }

            if (!receivedError)
            {
                scheduleTimer();
            }
            else
            {
                exitApplication(responseStr);
            }
        }

        private void backgroundWorker1_processScreenshot(object sender, DoWorkEventArgs e)
        {
            scheduleTimer();
        }

        private void storeAPIKey(string apiKey, string xmlPath)
        {
            //TODO: Need to check if document already exists. If it does, then
            //load the old document into memory and modify its nodes instead
            //of wiping out the old document.
            XDocument doc = new XDocument(
                new XElement("settings",
                    new XElement("apikey", apiKey)
                )
            );

            doc.Save(xmlPath);
        }

        private void startApplication()
        {
            //Hide form until it it determined that user needs to log in (settings file doesn't exist).
            this.Opacity = 0;

            interceptorDir = Path.Combine(
                System.Environment.GetEnvironmentVariable("USERPROFILE"),
                "Interceptor");

            //Make sure screenshots directory exists.
            screenshotDir = Path.Combine(interceptorDir, "screenshots");

            if (!Directory.Exists(screenshotDir))
            {
                try
                {
                    Directory.CreateDirectory(screenshotDir);
                }
                catch (Exception)
                {
                    exitApplication("Could not create folder " + screenshotDir + " Please create this folder and try restarting Interceptor.");
                }
            }

            //Log in (validate that user exists, has paid his/her subscription fees).
            settingsFile = Path.Combine(interceptorDir, "settings.xml");

            if (File.Exists(settingsFile))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(settingsFile);

                XmlNode apiKeyNode = doc.DocumentElement.SelectSingleNode("/settings/apikey");
                apiKey = apiKeyNode.InnerText;

                checkLogin();
            }
            else
            {
                //Show form if user needs to log in (settings file doesn't exist).
                this.Opacity = 100;
            }
        }

        private void startBackgroundWorker()
        {
            //Form to login should not be visible while background worker is running.
            this.Opacity = 0;
            screenshotBackGroundWorker.RunWorkerAsync();
        }

        private void exitApplication(string errorMsg)
        {
            MessageBox.Show(errorMsg);
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Only show form if user needs to log in.
            this.Opacity = 0;

            startApplication();
        }

        private void textLabel_Click(object sender, EventArgs e)
        {

        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void PasswordLabel_Click(object sender, EventArgs e)
        {

        }

        private void PasswordLabel_Click_1(object sender, EventArgs e)
        {

        }

        private string[] getHashedPasswordAndSalt(string password)
        {
            //Create salt value
            byte[] saltBytes;
            new RNGCryptoServiceProvider().GetBytes(saltBytes = new byte[16]);

            //Get unsalted hash value
            var pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes, 10000);
            byte[] hashBytes = pbkdf2.GetBytes(20);

            //Combine salt and hashed password.
            byte[] hashedPasswordBytes = new byte[36];
            Array.Copy(saltBytes, 0, hashedPasswordBytes, 0, saltBytes.Length);
            Array.Copy(hashBytes, 0, hashedPasswordBytes, saltBytes.Length, hashBytes.Length);

            //Base64 encode salt and hashed password for easier storage.
            string[] toReturn = {
                Convert.ToBase64String(hashedPasswordBytes),
                Convert.ToBase64String(saltBytes)
            };

            return toReturn;
        }

        private bool login()
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Text;

            //TODO: Contact login server, check response for success or failure.
            //Send login info to http://localhost:12345/customer_login using HTTP POST request.
            using (WebClient client = new WebClient())
            {
                var values = new NameValueCollection();

                values.Add("username", username);
                values.Add("password", password);

                try
                {
                    var responseBytes = client.UploadValues("http://localhost:12345/customer_login", values);

                    apiKey = System.Text.Encoding.UTF8.GetString(responseBytes, 0, responseBytes.Length);

                    Console.WriteLine("Login response: \"" + apiKey + "\"");

                    if (apiKey != "FAIL")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (WebException)
                {
                    return false;
                }
                catch (DecoderFallbackException)
                {
                    return false;
                }
            }
        }

        private void checkLogin()
        {
            if (login())
            {
                //If we needed to manually log in, then we haven't saved our API key. Save it in settings.
                storeAPIKey(apiKey, settingsFile);

                //User has logged in. Start background worker.
                startBackgroundWorker();
            }
            else
            {
                //Show form if user needs to log in (password and username are incorrect).
                this.Opacity = 100;
                MessageBox.Show("Sorry! Your username or password was incorrect. Please try again.");
            }
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            checkLogin();
        }

        private void UsernameTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
